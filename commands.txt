================================================================================
// Start a project

cmd : django-admin startproject [project_name]
================================================================================



================================================================================
// Run Project

cmd : python manage.py runserver
================================================================================



================================================================================
// Migrations

If you want to give the migration(s) 
a meaningful name instead of a generated one, 
you can use the makemigrations --name option:

cmd : python manage.py makemigrations --name changed_my_model your_app_label

To perform migration:
cmd : python manage.py migrate
================================================================================





================================================================================
// App Creation

If you want to create an app in project

cmd : python manage.py startapp [app_name]

================================================================================