from django.shortcuts import render
from .forms import MessageForm

# Create your views here.

def index(request):
    return render(request, 'basic_app/index.html')


def about(request):
    return render(request, 'basic_app/about.html')


def contact(request):
    form = MessageForm()

    if request.method == 'POST':
        form = MessageForm(request.POST)

        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            message = form.cleaned_data['message']


            print("Validation success")
            print(f'{name} {email} {message}')
            return render(request, 'basic_app/about.html', {'name' : name, 'email' : email, 'message' : message,})

    return render(request, 'basic_app/contact.html', {'form' : form})
